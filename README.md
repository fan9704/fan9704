## Welcome To FKT's Profile

----

## 👉 About Me

<div style="text-align:center;">
<a href="https://stackoverflow.com/users/18726758/fkt"><img  style="background:white;display:inline; width:24%;" src="https://cdn.sstatic.net/Sites/stackoverflow/Img/apple-touch-icon@2.png?v=73d79a89bded"> </img></a>
<a href="https://medium.com/@cxz123499"><img  style="display:inline; width:24%;" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRouQDiWR9LkKE1ms5eaK5cJMjcn76VvNmaLA&usqp=CAU"> </img></a>
<a href="https://www.cloudskillsboost.google/public_profiles/2192f42f-459d-4169-88b5-903944e66697"><img  style="display:inline; width:24%;" src="https://media-exp1.licdn.com/dms/image/C510BAQFR04KgVWnd3w/company-logo_200_200/0/1519878429204?e=2147483647&v=beta&t=Gpyo1sg5WO5_hF1DlDmVasu7116-7DNymlohoRA7qmI"> </img></a>
<a href="https://www.facebook.com/tim.frank.969/"><img  style="display:inline;  width:24%; " src="https://i.imgur.com/tjuKgaO.png"> </img></a>
</div>

----

## ⚡ Technologies

![](https://i.imgur.com/MHde8Kv.png)
![](https://i.imgur.com/d1M8Ma0.png)
![](https://i.imgur.com/xs6GuAv.png)
![](https://i.imgur.com/aI6x8YR.png)
![](https://i.imgur.com/9luhOhi.png)

----

## 📊 Statistics

<div style="display:flex;">
<img style="display:inline-block;width:52%;" src="https://github-readme-stats.vercel.app/api?username=fan9704&count_private=true&show_icons=true&theme=gotham&include_all_commits=true">
</img>
<img style="display:inline-block;width:44%;" src="https://github-readme-stats.vercel.app/api/top-langs/?username=fan9704&theme=gotham&layout=compact">
</img></div>

<span align="center">

![Visitor Badge](https://visitor-badge.laobi.icu/badge?page_id=fan9704.fan9704)

</span>
